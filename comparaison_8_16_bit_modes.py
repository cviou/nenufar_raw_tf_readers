#!/usr/bin/env python3

###############################################################################
#
# Copyright (C) 2019
# Station de Radioastronomie de Nançay,
# Observatoire de Paris, PSL Research University, CNRS, Univ. Orléans, OSUC,
# 18330 Nançay, France
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
# Author: Cedric Viou (Cedric.Viou@obs-nancay.fr)
# https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/doc/serveurs/nenufar-tf/
# https://gitlab.obspm.fr/cviou/nenufar_raw_tf_readers
#
# Description:
# Comparaison of 8-bit VS 16-bit bit_mode raw BHR streams
#
###############################################################################

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import nenufar_raw

run_batch = True

time_analysis = True

spectral_analysis = True
do_acc = True

raw_16 = nenufar_raw.raw('/databf2/nenufar-tf/ES00/2020/03/20200325_051000_20200325_051600_TEST_RAW_16',
                         verbose=True,
                         )
# get mmap handle on data files
dat16 = raw_16.raw[0]['data']

output_dir = Path('./')
output_file = output_dir / "RAW_16_as_int8.raw"


# general parameters
block_num = 6
Nof_pol = 2
ReIm = 2
eps = np.finfo(float).eps
fftlen = 256


if time_analysis:
    wave16 = dat16[block_num].copy()

if spectral_analysis: 
    # process 16-bit data
    header = raw_16.header[0]
    nof_blocks = dat16.shape[0]
    nobpb = header['nobpb']
    nfft = dat16.shape[1] // (nobpb * fftlen * Nof_pol * ReIm)
    tmp = dat16[block_num].copy()
    tmp.shape = (nfft, fftlen, nobpb, Nof_pol * ReIm)
    tmp = tmp.astype('float32').view('complex64')
    tmp = tmp.transpose((3, 0, 2, 1))
    TMP = np.fft.fft(tmp, axis=-1)
    TMP = np.fft.fftshift(TMP, axes=-1)
    TMP.shape = (Nof_pol, nfft, nobpb*fftlen)
    DAT16 = TMP
    DAT16_DAT16 = DAT16.real**2 + DAT16.imag**2


if run_batch:
    # get or generate 8-bit data from 16-bit data
    batch = [
             ("save_file = 'gain=0.125_as_float_no_clipping.png'",         "rs = 3",
              "tmp = dat16[block_num].copy() / 2**rs"
              ),
             ("save_file = 'gain=0.125_as_float_16_to_8_clipping.png'",  "rs = 3",
              "tmp = dat16[block_num].copy() / 2**rs",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.125_as_int_no_clipping.png'",           "rs = 3",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              ),
             ("save_file = 'gain=0.125_as_int_16_to_8_clipping.png'",    "rs = 3",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.125_RNE_16_to_8_clipping.png'",       "rs = 3",
              "raw_16.write_16_as_8bit_raw(output_file, lane=0, rs=rs, start=0, end=8)",
              "raw_16_as_8 = nenufar_raw.raw(output_dir, verbose=True)",
              "dat8 = raw_16_as_8.raw[0]['data']",
              "tmp = dat8[block_num].copy()",
              ),

             ("save_file = 'gain=0.25_as_float_no_clipping.png'",          "rs = 2",
              "tmp = dat16[block_num].copy() / 2**rs",
              ),
             ("save_file = 'gain=0.25_as_float_16_to_8_clipping.png'",   "rs = 2",
              "tmp = dat16[block_num].copy() / 2**rs",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.25_as_int_no_clipping.png'",            "rs = 2",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              ),
             ("save_file = 'gain=0.25_as_int_16_to_8_clipping.png'",     "rs = 2",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.25_RNE_16_to_8_clipping.png'",        "rs = 2",
              "raw_16.write_16_as_8bit_raw(output_file, lane=0, rs=rs, start=0, end=8)",
              "raw_16_as_8 = nenufar_raw.raw(output_dir, verbose=True)",
              "dat8 = raw_16_as_8.raw[0]['data']",
              "tmp = dat8[block_num].copy()",
              ),

             ("save_file = 'gain=0.50_as_float_no_clipping.png'",          "rs = 1",
              "tmp = dat16[block_num].copy() / 2**rs",
              ),
             ("save_file = 'gain=0.50_as_float_16_to_8_clipping.png'",   "rs = 1",
              "tmp = dat16[block_num].copy() / 2**rs",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.50_as_int_no_clipping.png'",            "rs = 1",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              ),
             ("save_file = 'gain=0.50_as_int_16_to_8_clipping.png'",     "rs = 1",
              "tmp = np.round(dat16[block_num].copy() / 2**rs)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'gain=0.50_RNE_16_to_8_clipping.png'",        "rs = 1",
              "raw_16.write_16_as_8bit_raw(output_file, lane=0, rs=rs, start=0, end=8)",
              "raw_16_as_8 = nenufar_raw.raw(output_dir, verbose=True)",
              "dat8 = raw_16_as_8.raw[0]['data']",
              "tmp = dat8[block_num].copy()",
              ),

             ("save_file = 'noRFI_gain=0.25_as_float_no_clipping.png'",          "rs = 2",
              "wave16 = dat16[block_num].copy()",
              "wave16.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "beamlet_idx = 0",
              "nobpb = 1",
              "wave16 = wave16[:,beamlet_idx,:].copy()",
              "wave16.shape = (1,-1)",
              "tmp = dat16[block_num].copy() / 2**rs",
              "tmp.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "tmp = tmp[:,beamlet_idx,:].copy()",
              "tmp.shape = (1,-1)",
              ),
             ("save_file = 'RFI_gain=0.25_as_float_no_clipping.png'",          "rs = 2",
              "wave16 = dat16[block_num].copy()",
              "wave16.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "beamlet_idx = 11",
              "nobpb = 1",
              "wave16 = wave16[:,beamlet_idx,:].copy()",
              "wave16.shape = (1,-1)",
              "tmp = dat16[block_num].copy() / 2**rs",
              "tmp.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "tmp = tmp[:,beamlet_idx,:].copy()",
              "tmp.shape = (1,-1)",
              ),
             ("save_file = 'noRFI_gain=0.25_as_int_16_to_8_clipping.png'",          "rs = 2",
              "wave16 = dat16[block_num].copy()",
              "wave16.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "beamlet_idx = 0",
              "nobpb = 1",
              "wave16 = wave16[:,beamlet_idx,:].copy()",
              "wave16.shape = (1,-1)",
              "tmp = dat16[block_num].copy() / 2**rs",
              "tmp.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "tmp = tmp[:,beamlet_idx,:].copy()",
              "tmp.shape = (1,-1)",
              "tmp = np.round(tmp)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ("save_file = 'RFI_gain=0.25_as_int_16_to_8_clipping.png'",          "rs = 2",
              "wave16 = dat16[block_num].copy()",
              "wave16.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "beamlet_idx = 11",
              "wave16 = wave16[:,beamlet_idx,:].copy()",
              "wave16.shape = (1,-1)",
              "tmp = dat16[block_num].copy() / 2**rs",
              "tmp.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "tmp = tmp[:,beamlet_idx,:].copy()",
              "tmp.shape = (1,-1)",
              "nobpb = 1",
              "tmp = np.round(tmp)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),

             ]
else:
    # Rewrites raw_16 as int8 data
    batch = [
             #("save_file = 'test.png'",          "rs = 2",
             # "tmp = dat16[block_num].copy() / 2**rs",
             # # "tmp = np.round(tmp)",
             # #"tmp = round_half_away_from_zero(tmp)",
             # #"tmp = round_half_toward_zero(tmp)",
             # ),
             ("save_file = 'test.png'",          "rs = 2",
              "wave16 = dat16[block_num].copy()",
              "wave16.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "beamlet_idx = 11",
              "wave16 = wave16[:,beamlet_idx,:].copy()",
              "wave16.shape = (1,-1)",
              # "wave16[0,0:2] = np.array((-5000,5000))",
              "tmp = dat16[block_num].copy() / 2**rs",
              "tmp.shape = (-1, raw_16.header[0]['nobpb'], Nof_pol * ReIm)",
              "tmp = tmp[:,beamlet_idx,:].copy()",
              "tmp.shape = (1,-1)",
              "nobpb = 1",
              "tmp = np.round(tmp)",
              "np.clip(tmp, -128, 127, out=tmp)",
              ),
             ]

    import math
    
    def round_half_up(n, decimals=0):
        multiplier = 10 ** decimals
        return math.floor(n*multiplier + 0.5) / multiplier
    
    def round_half_down(n, decimals=0):
        multiplier = 10 ** decimals
        return math.ceil(n*multiplier - 0.5) / multiplier
    
    def round_half_away_from_zero(n, decimals=0):
        rounded_abs = round_half_up(abs(n), decimals)
        return math.copysign(rounded_abs, n)
    
    def round_half_toward_zero(n, decimals=0):
        rounded_abs = round_half_down(abs(n), decimals)
        return math.copysign(rounded_abs, n)
    
    round_half_away_from_zero = np.vectorize(round_half_away_from_zero)
    round_half_toward_zero = np.vectorize(round_half_toward_zero)
    

for procs in batch:
    for proc in procs:
        print(f"Running {proc}")
        exec(proc)
    print("")    

    f = np.arange(nobpb * fftlen) / fftlen


    if time_analysis:
        def bar2plot(histo_bins):
            histo, bins = histo_bins
            pbins = np.tile(bins, (2,1)).transpose().reshape((1,-1))[0]
            phist = np.hstack((0, np.tile(histo, (2,1)).transpose().reshape((1,-1))[0], 0))
            return phist, pbins

        wave8 = tmp.astype('float64').copy()
        min16, max16 = wave16.min(), wave16.max()
        min16 = min(min16, -128 * 2**rs)
        max16 = max(max16, 127 * 2**rs)
        bin_width = 1
        hist16, bins16 = np.histogram(wave16, np.arange(min16, max16, bin_width))
        hist8 , bins8  = np.histogram(wave8, np.arange(min16, max16, bin_width))
        N_samples = hist16.sum()

        phist16, pbins16 = bar2plot((hist16, bins16))
        phist8, pbins8 = bar2plot((hist8, bins8))

        fig1, axs = plt.subplots(1, 2, figsize=(12,10))
        plot_params = {'alpha': 0.8, }
        axs[0].plot(pbins16, phist16 / N_samples, **plot_params)
        axs[0].plot(2**rs * pbins8, phist8 / N_samples / 2**rs, **plot_params)
        axs[0].set_xlim((min16, max16))
        axs[0].set_xlabel("RAW 16 bins")
        axs[0].set_ylabel(r"$\hat{PDF}$")
        axs[0].legend(('RAW16', 'RAW8'))

        ax1 = axs[0].twiny()
        new_tick_locations = np.array((-128, 0, 127)) * 2**rs
        ax1.set_xlim(axs[0].get_xlim())     
        ax1.set_xticks(new_tick_locations)
        ax1.set_xticklabels(new_tick_locations / 2**rs)
        ax1.set_xlabel("RAW 8 bins")
        ax1.grid(True)

        axs[1].plot(pbins16, phist16 / N_samples, **plot_params)
        axs[1].plot(2**rs * pbins8, phist8 / N_samples / 2**rs, **plot_params)
        axs[1].set_xlim((-30, 30))
        axs[1].set_xlabel("RAW 16 bins")
        axs[1].set_ylabel(r"$\hat{PDF}$")
        axs[1].text(0, phist16.max() / N_samples / 3, 'ZOOM', bbox=dict(facecolor='red', alpha=0.5))
        axs[1].legend(('RAW16', 'RAW8'))

        ax2 = axs[1].twiny()
        zoom_ax_lim = axs[1].get_xlim()
        new_tick_locations = (2**rs * pbins8)
        new_tick_locations = new_tick_locations[  (new_tick_locations > zoom_ax_lim[0])
                                                & (new_tick_locations < zoom_ax_lim[1])]
        ax2.set_xlim(zoom_ax_lim)     
        ax2.set_xticks(new_tick_locations)
        ax2.set_xticklabels(new_tick_locations / 2**rs)
        ax2.set_xlabel("RAW 8 bins")
        ax2.grid(True)

        fig1.suptitle(save_file)
        fig1.tight_layout()
        fig1.savefig("/home/cedric.viou/8vs16_bitmode/histo_" + save_file)



    if spectral_analysis: 
 
        if nobpb == 1:
            tmp16 = dat16[block_num].copy()
            tmp16.shape = (nfft, fftlen, raw_16.header[0]['nobpb'], Nof_pol * ReIm)
            tmp16 = tmp16[:,:,beamlet_idx:beamlet_idx+1,:].astype('float32').view('complex64')
            tmp16 = tmp16.transpose((3, 0, 2, 1))
            TMP16 = np.fft.fft(tmp16, axis=-1)
            TMP16 = np.fft.fftshift(TMP16, axes=-1)
            TMP16.shape = (Nof_pol, nfft, nobpb*fftlen)
            DAT16 = TMP16
            DAT16_DAT16 = DAT16.real**2 + DAT16.imag**2

            # process 8-bit data to compute TF plannes or accumulated spectra
            tmp.shape = (nfft, fftlen, nobpb, Nof_pol * ReIm)
            tmp = tmp[:,:,beamlet_idx:beamlet_idx+1,:].astype('float64').view('complex128')
            tmp = tmp.transpose((3, 0, 2, 1))
            TMP = np.fft.fft(tmp, axis=-1)
            TMP = np.fft.fftshift(TMP, axes=-1)
            TMP.shape = (Nof_pol, nfft, nobpb*fftlen)
            DAT8 = TMP * 2**rs
            DAT8_DAT8 = DAT8.real**2 + DAT8.imag**2
        else:
            # process 8-bit data to compute TF plannes or accumulated spectra
            tmp.shape = (nfft, fftlen, nobpb, Nof_pol * ReIm)
            tmp = tmp.astype('float64').view('complex128')
            tmp = tmp.transpose((3, 0, 2, 1))
            TMP = np.fft.fft(tmp, axis=-1)
            TMP = np.fft.fftshift(TMP, axes=-1)
            TMP.shape = (Nof_pol, nfft, nobpb*fftlen)
            DAT8 = TMP * 2**rs
            DAT8_DAT8 = DAT8.real**2 + DAT8.imag**2
    
    
    
        fig1, axs = plt.subplots(4, 2, figsize=(12,10))
        title = ('16-bit data',
                 '8-bit data',
                 'diff 16/8 bit (dB)',
                 'rel_err (%)')
        v = (55, 105)
        for idx_dat, dat in enumerate((DAT16_DAT16, DAT8_DAT8)):
            for polar in (0, 1):
                data_plot = dat[polar, :, :]
                if do_acc:
                    data_plot = data_plot.mean(axis=(0,))
                    data_plot = 10 * np.log10(data_plot+eps)
                    axs[idx_dat,polar].plot(f, data_plot)
                    axs[idx_dat,polar].set_xlabel("Beamlet #")
                    axs[idx_dat,polar].set_ylabel("dB")
                    axs[idx_dat,polar].set_xlim((0, nobpb))
                    axs[idx_dat,polar].set_ylim(v)
                    axs[idx_dat,polar].xaxis.set_ticks(np.arange(nobpb) + 0.5)
                    axs[idx_dat,polar].xaxis.set_ticklabels(np.arange(nobpb))
                    axs[idx_dat,polar].xaxis.set_tick_params(which='both', labelbottom=True)
                else:
                    data_plot = 10 * np.log10(data_plot+eps)
                    axs[idx_dat,polar].imshow(data_plot,
                                              extent=(f[0], f[-1], 0, 1),
                                              interpolation='nearest',
                                              origin='upper',
                                              vmin=v[0], vmax=v[1],
                                              cmap="Greys_r"
                                              )
                    axs[idx_dat,polar].axis('tight')
    
                axs[idx_dat,polar].set_title(f"{title[idx_dat]}"
                                             f" pol {('X','Y')[polar]}")
    
        for polar in (0, 1):
            if do_acc:
                d16 = DAT16_DAT16[polar, :, :].mean(axis=(0,))
                d8 = DAT8_DAT8[polar, :, :].mean(axis=(0,))
                d16 = 10 * np.log10(d16+eps)
                d8 = 10 * np.log10(d8+eps)
                axs[2,polar].plot(f, d16 - d8)
                v_diff = (-0.025, 0.025)
                axs[2,polar].set_xlabel("Beamlet #")
                axs[2,polar].set_ylabel("dB")
                axs[2,polar].set_xlim((0, nobpb))
                axs[2,polar].set_ylim(v_diff)
                axs[2,polar].xaxis.set_ticks(np.arange(nobpb) + 0.5)
                axs[2,polar].xaxis.set_ticklabels(np.arange(nobpb))
                axs[2,polar].xaxis.set_tick_params(which='both', labelbottom=True)
            else:
                v_diff = (-1, 1)
                data_plot = 10 * np.log10(DAT16_DAT16[polar, :, :]+eps) - 10 * np.log10(DAT8_DAT8[polar, :, :]+eps)
                axs[2,polar].imshow(data_plot,
                                          extent=(f[0], f[-1], 0, 1),
                                          interpolation='nearest',
                                          origin='upper',
                                          vmin=v_diff[0], vmax=v_diff[1],
                                          cmap="Greys_r"
                                          )
                axs[2,polar].axis('tight')
    
            axs[2,polar].set_title(f"{title[2]}"
                                   f" pol {('X','Y')[polar]}")
    
        for polar in (0, 1):
            if do_acc:
                d16 = DAT16_DAT16[polar, :, :].mean(axis=(0,))
                d8 = DAT8_DAT8[polar, :, :].mean(axis=(0,))
                axs[3,polar].plot(f, 100*(d16 - d8) / d16)
                v_diff = (-1,1)
                axs[3,polar].set_xlabel("Beamlet #")
                axs[3,polar].set_ylabel("%")
                axs[3,polar].set_xlim((0, nobpb))
                axs[3,polar].set_ylim(v_diff)
                axs[3,polar].xaxis.set_ticks(np.arange(nobpb) + 0.5)
                axs[3,polar].xaxis.set_ticklabels(np.arange(nobpb))
                axs[3,polar].xaxis.set_tick_params(which='both', labelbottom=True)
            else:
                v_diff = (-5, 5)
                data_plot = 100 * (DAT16_DAT16[polar, :, :] - DAT8_DAT8[polar, :, :]) / DAT16_DAT16[polar, :, :]
                axs[3,polar].imshow(data_plot,
                                          extent=(f[0], f[-1], 0, 1),
                                          interpolation='nearest',
                                          origin='upper',
                                          vmin=v_diff[0], vmax=v_diff[1],
                                          cmap="Greys_r"
                                          )
                axs[3,polar].axis('tight')
    
            axs[3,polar].set_title(f"{title[3]}"
                                   f" pol {('X','Y')[polar]}")
    
        fig1.suptitle(save_file)
        fig1.tight_layout()
        fig1.savefig("/home/cedric.viou/8vs16_bitmode/spectro_" + save_file)

plt.show()
    
