#!/usr/bin/env python3

###############################################################################
#
# Copyright (C) 2019
# Station de Radioastronomie de Nançay,
# Observatoire de Paris, PSL Research University, CNRS, Univ. Orléans, OSUC,
# 18330 Nançay, France
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
# Author: Cedric Viou (Cedric.Viou@obs-nancay.fr)
# https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/doc/serveurs/nenufar-tf/
# https://gitlab.obspm.fr/cviou/nenufar_raw_tf_readers
#
# Description:
# Lib that can read nenufar-tf output files
#
###############################################################################
# TODO:
#  - Improve doc strings
###############################################################################



from pathlib import Path
import numpy as np
import re
from astropy.time import Time
import sys

#pip3 install --user dateparser
#import dateparser

max_bsn = 200e6 / 1024
apods = {  0: 'Square',
         101: 'Hamming',
         102: 'Hann',
         199: '1/2 Square',
         }


class spectra:
    __slots__ = ['obs_dir', 'verbose',
                 'fnames_info', 'fnames_log', 'fnames_data',
                 'lane', 'beam', 'parset', 'manip_dir',
                 'obs_name', 'manip_dir',
                 ]

    def __init__(self,
                 obs_name,
                 obs_dir="/databf2/nenufar-tf/",
                 manip_dir="/databf2/nenufar/",
                 verbose=False,
                 ):
        self.obs_name = obs_name
        self.obs_dir = Path(obs_dir) / self.obs_name
        year, month = self.obs_name[0:4], self.obs_name[4:6]
        self.manip_dir = Path(manip_dir) / year / month / self.obs_name
        assert self.obs_dir.exists(), "File not found.  Check path, or try running on a nancep node"
        assert self.manip_dir.exists(), "File not found.  Check path, or try running on a nancep node"
        self.verbose = verbose
        self.fnames_info = sorted(list(self.obs_dir.glob("*.spectra.info")))
        self.fnames_log = sorted(list(self.obs_dir.glob("*.spectra.log")))
        self.fnames_data = sorted(list(self.obs_dir.glob("*.spectra")))
        self.lane = {}
        self.beam = []
        self.parset = None

        if self.verbose:
            print("Manip directory %s" % self.manip_dir.name)
            [print(f.name) for f in sorted(list(self.manip_dir.glob("*.*")))]
            print("")

        self.parse_parset(list(self.manip_dir.glob("*.parset"))[0])

        for (fn_info, fn_data) in zip(self.fnames_info,
                                      self.fnames_data):
            self.parse_info_data(fn_info, fn_data)


    def parse_parset(self, fn_parset):
        """Extract info from parset file"""
        re_beam = re.compile(r'^(Beam|AnaBeam)\[(\d+)\]')
        re_lane = re.compile(r'^lane(\d+)$')
        re_lane_ind = re.compile(r'^lane(\d+)_ind$')
        re_subbandList = re.compile(r'^subbandList$')
        re_slice = re.compile(r'^\[(\d+)..(\d+)\]$')

        with fn_parset.open('r') as fid:
            parset = [l.strip().split('=', maxsplit=1) for l in fid.readlines()
                                                       if l != "\n"]

            tmp = {}
            for l in parset:
                (section, subsection), value = l[0].split('.'), l[1]
                is_beam = re_beam.match(section)
                if is_beam:
                    beam_type, beam_number = is_beam.group(1), int(is_beam.group(2))
                    try:
                        tmp[beam_type][beam_number][subsection] = value
                    except KeyError:
                        tmp[beam_type] = [{}, ]
                        tmp[beam_type][beam_number][subsection] = value
                    except IndexError:
                        tmp[beam_type].append({})
                        tmp[beam_type][beam_number][subsection] = value
                else:
                    try:
                        tmp[section][subsection] = value
                    except KeyError:
                        tmp[section] = {}
                        tmp[section][subsection] = value

        # Create a dict out of the lanes found in each beams
        for beam_idx, beam in enumerate(tmp['Beam']):
            beam['lane'] = {}
            beam['lane_ind'] = {}

            # Collect apodisation window for given beam
            fname_apodisation = list(self.obs_dir.glob("*%d.apodisation" % beam_idx))
            if len(fname_apodisation) == 1:
                beam['apodisation'] = np.loadtxt(fname_apodisation[0].as_posix())[:, 1:3]
                if self.verbose:
                    print("Found apodisation file %s" % (fname_apodisation[0]))

            for k, v in beam.items():
                is_lane = re_lane.match(k)
                if is_lane:
                    is_slice = re_slice.match(v)
                    if is_slice:
                        start_stop = [int(l) for l in is_slice.groups()]
                        beam['lane'][int(is_lane.group(1))] = start_stop

                is_lane_ind = re_lane_ind.match(k)
                if is_lane_ind:
                    beam['lane_ind'][int(is_lane_ind.group(1))] = int(v)

                is_subbandList = re_subbandList.match(k)
                if is_subbandList:
                    is_slice = re_slice.match(v)
                    if is_slice:
                        start, stop = [int(l) for l in is_slice.groups()]
                        beam['subbandList'] = np.arange(start, stop+1)
            # create a logical assignment table for each beamlet of each beam
            beam['logical'] = np.full((4, 192), -1, dtype=int)
            beam['f0'] = beam['subbandList'] * max_bsn
            for lane_idx in beam['lane'].keys():
                start_pos = beam['lane_ind'][lane_idx]
                start_val, stop_val = beam['lane'][lane_idx]
                stop_pos = start_pos + (stop_val - start_val + 1)
                beam['logical'][lane_idx, start_pos:stop_pos] = np.arange(start_val, stop_val+1)
        self.parset = tmp

    def parse_info_data(self, fn_info, fn_data):
        """get post processing parameters"""
        if self.verbose:
            print("Parsing %s + info file" % (fn_data.name))

        with fn_info.open('r') as fd_info:
            line = fd_info.readline()
            m = re.search('^nbchan=\s*(\d+)\s+fftlen=\s*(\d+)\s+nfft2int=\s*(\d+)\s+nffte=\s*(\d+)\s+ovlp=\s*(\d+)\s+apod=\s*(\d+)\s+.*', line)
            nof_beamlets, fftlen, nfft2int, nffte, ovlp, apod = map(int, m.groups())

            lane = []
            times = []
            chans = []
            beam = []

            # first block
            line = fd_info.readline()
            re_blk = re.compile('^blk=\s*(\d+)\s+eiss=\s*(\d+)\s+tss=\s*(\d+)\s+bsns=\s*(\d+)\s+t=\s*(\d+\.\d+)\s*$')
            m = re_blk.search(line)
            blk, eiss, tss, bsns, t = m.groups()
            (blk, eiss, tss, bsns), t = map(int, (blk, eiss, tss, bsns)), float(t)
            times.append(float(tss) + bsns / max_bsn)

            # parse ichans (subband number)
            line = fd_info.readline()
            while line.startswith(" ichan= "):
                line = map(int, line.split(' ')[2:])
                chans.extend(line)
                line = fd_info.readline()

            # parse lane number
            while line.startswith(" ilane= "):
                line = map(int, line.split(' ')[2:])
                lane.extend(line)
                line = fd_info.readline()

            # parse beam number
            while line.startswith(" ibeam= "):
                line = map(int, line.split(' ')[2:])
                beam.extend(line)
                line = fd_info.readline()

            # skip f_min/f_max
            line = fd_info.readline()

            # skip mjd/greg
            line = fd_info.readline()

            # other blocks
            while line:
                m = re_blk.search(line)
                if m:
                    blk, eiss, tss, bsns, t = m.groups()
                    blk, eiss, tss, bsns = map(int, (blk, eiss, tss, bsns))
                    t = float(t)
                    times.append(float(tss) + bsns / max_bsn)
                line = fd_info.readline()

        lane = list(set(lane))
        assert len(lane) == 1
        lane = lane[0]
        self.lane[lane] = {}
        self.lane[lane]['fn_info'] = fn_info
        self.lane[lane]['fn_data'] = fn_data
        self.lane[lane]['nof_beamlets'] = nof_beamlets
        self.lane[lane]['fftlen'] = fftlen
        self.lane[lane]['nfft2int'] = nfft2int
        self.lane[lane]['nffte'] = nffte
        self.lane[lane]['ovlp'] = ovlp
        self.lane[lane]['apod'] = apods[apod]
        self.lane[lane]['chans'] = np.array(chans)
        self.lane[lane]['beamlet_f0'] = (np.array(chans) * max_bsn)
        self.lane[lane]['beam'] = list(set(beam))
        self.lane[lane]['times'] = np.array(times)
        self.lane[lane]['nof_blocks'] = len(times)

        dt_block = self.infer_data_structure(fn_data)
        self.lane[lane]['dt_block'] = dt_block
        self.lane[lane]['fn_data'] = self.open_spectra(fn_data, dt_block)
        self.lane[lane]['PFB_corr'] = self.load_PFB_corr(fftlen)**2

        if self.verbose:
            print("{} -> nbchan={}, fftlen={}, nfft2int={}".format(fn_info.name,
                                                                   nof_beamlets,
                                                                   fftlen,
                                                                   nfft2int,
                                                                   ))


    def infer_data_structure(self, fn_data):
        """Parse first binary file to build the block structure definition
        Description of .spectra files as produced by 'tf' code with undysputed           (Feb 11, 2019)
        /-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/
        for each block
         ULONG    first effective sample index as incremented by the 'tf' code
                    used for this block (starting with 0!)
         ULONG    TIMESTAMP of first sample (see NenuFAR/LOFAR beamlet description)
         ULONG    BLOCKSEQNUMBER of first sample (see NenuFAR/LOFAR beamlet description)
         LONGINT  fftlen : FFT length -> freq resolution= 0.1953125/fftlen MHz
         LONGINT  nfft2int : Nber of integrated FFTs -> time resolution= fftlen*nfft2int*5.12 us
         LONGINT  ffovlp : FFToverlap 0/1                    (not used yet)
         LONGINT  apodisation : (0: none, 54=hamming, ...)   (not used yet)
         LONGINT  nffte : nber of FFTs to be read per beamlet within this block
                     (so we have 'nffte' spectra/time_sample per block)
         LONGINT  nbchan : nbr of subsequent beamlets/channels (nb of NenuFAR/LOFAR 195kHz beamlets)
                     (so we have 'fftlen*nbchan' frequencies for each spectra)

         for each beamlet
          LONGINT  lane index (0, 1, 2 or 3)
          LONGINT  beam index (numerical beam : 0, 1, ...)
          LONGINT  beamlet/channel index (0...768, 0 is 0.000MHz)
          FLOAT    pol0 data : 'nffte' time_samples each made of 'fftlen' pairs [XrXr+XiXi : YrYr+YiYi]
          FLOAT    pol1 data : 'nffte' time_samples each made of 'fftlen' pairs [XrYr+XiYi : XrYi-XiYr]
        /-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/
        """
        dt_block = np.dtype([('idx', 'uint64'),
                             ('TIMESTAMP', 'uint64'),
                             ('BLOCKSEQNUMBER', 'uint64'),
                             ('fftlen', 'int32'),
                             ('nfft2int', 'int32'),
                             ('fftovlp', 'int32'),
                             ('apodisation', 'int32'),
                             ('nffte', 'int32'),
                             ('nbchan', 'int32'),
                             ])

        fd_data = fn_data.open('rb')
        header = np.frombuffer(fd_data.read(dt_block.itemsize),
                               count=1,
                               dtype=dt_block,
                               )[0]
        fd_data.close()

        nffte = header['nffte']
        fftlen = header['fftlen']
        nbchan = header['nbchan']

        dt_channel = np.dtype([('lane', 'int32'),
                               ('beam', 'int32'),
                               ('channel', 'int32'),
                               ('fft0', 'float32', (nffte, fftlen, 2)),
                               ('fft1', 'float32', (nffte, fftlen, 2)),
                               ])

        dt_block = np.dtype([('idx', 'uint64'),
                             ('TIMESTAMP', 'uint64'),
                             ('BLOCKSEQNUMBER', 'uint64'),
                             ('fftlen', 'int32'),
                             ('nfft2int', 'int32'),
                             ('fftovlp', 'int32'),
                             ('apodisation', 'int32'),
                             ('nffte', 'int32'),
                             ('nbchan', 'int32'),
                             ('data', dt_channel, (nbchan)),
                             ])

        return dt_block

    def load_PFB_corr(self, fftlen):
        try:
            PFB_corr = np.loadtxt('./LOFAR-Bandpass-Correction-nfft-%03d.dat' % fftlen,
                                  dtype=np.float32)
        except FileNotFoundError as msg:
            PFB_corr = 1
            print(msg)
        return PFB_corr

    def compute_PFB_corr(self, fftlen):
        # compute Gain correction for PFB band pass
        Kaiser = np.loadtxt('Coeffs16384Kaiser-quant.dat')
        N_tap = 16
        chan_w = fftlen
        Over_sampling = chan_w//N_tap
        n_fft = Over_sampling * Kaiser.shape[0]

        g_high_res = np.fft.fft(Kaiser, n_fft)
        middle = np.r_[g_high_res[-chan_w // 2:], g_high_res[:chan_w // 2]]
        right = g_high_res[chan_w//2:chan_w//2+chan_w]
        left = g_high_res[-chan_w//2-chan_w:-chan_w//2]

        g = 2**25/np.sqrt(abs(middle)**2+abs(left)**2+abs(right)**2)
        return g

    def save_PFB_corr(self, fftlen):
        g = self.compute_PFB_corr(fftlen)
        g.tofile('./LOFAR-Bandpass-Correction-nfft-%03d.dat' % fftlen,
                 sep="\n",
                 format="%.12f")

    def open_spectra(self, fn_data, dt_block):
        """Return a memory mapped file array"""
        tmp = np.memmap(fn_data.as_posix(), dtype='int8', mode='r')
        # Reinterpret an integer number of dt_block-length samples as dt_block
        nof_blocks = tmp.size * tmp.itemsize // (dt_block.itemsize)
        data = tmp[: nof_blocks * dt_block.itemsize].view(dt_block)
        return data

    def extract_data(self,
                     beam=0,
                     t=None,
                     f=None,
                     n=None,
                     k=None,
                     part='I',
                     fftshift=True,
                     PFB_correction=True):
        """Extract a chunk of data into a contiguous array.
        Data extraction is performed on a by block and beamlet quanta

        t can be unix timestamp, or str as '%Y-%m-%d %H:%M:%S.%f'.
        ex: '2019-02-12 12:30:45.123'

        part can be any Stokes (I, Q, U, V),
        or XX*, YY*, XY*
        """

        assert 0 <= beam < len(self.parset['Beam']), \
            "Wrong beam index"
        assert t is not None or n is not None, \
            "Choose time-slice with one of t or n"
        assert f is not None or k is not None, \
            "Choose frequency-slice with one of f or k"
        assert not(t is not None and n is not None), \
            "Choose time-slice with t or n, not both"
        assert not(f is not None and k is not None), \
            "Choose frequency-slice with f or k, not both"

        n_start, n_stop = n if n is not None else (None, None)
        k_start, k_stop = k if k is not None else (None, None)

        # get the fisrt lane of that beam
        lane0 = list(self.parset['Beam'][beam]['lane'].keys())[0]

        if t is not None:
            t_start, t_stop = t
            if type(t_start) is str:
                t_start = Time(t_start, format='isot', scale='utc')
                t_start = t_start.unix
            if type(t_stop) is str:
                t_stop = Time(t_stop, format='isot', scale='utc')
                t_stop = t_stop.unix

            n_start = self.lane[lane0]['times'].searchsorted(t_start)
            n_stop = self.lane[lane0]['times'].searchsorted(t_stop)+1

        for lane in self.parset['Beam'][beam]['lane'].keys():
            assert 0 <= n_start < self.lane[lane]['nof_blocks'], \
                "Start time not in datafile"
            assert 0 <= n_stop < self.lane[lane]['nof_blocks'], \
                "Stop time not in datafile"

        k_min, k_max = self.parset['Beam'][beam]['subbandList'][[0, -1]]

        if f is not None:
            f_start, f_stop = f
            k_start = k_min + self.parset['Beam'][beam]['f0'].searchsorted(f_start)
            k_stop  = k_min + self.parset['Beam'][beam]['f0'].searchsorted(f_stop)
        else:
            if k_stop == -1:
                k_stop = k_max + 1
            else:
                k_stop = min(k_max+1, k_stop)

            k_start = max(k_min, k_start)

        if n_stop == -1:
            n_stop = self.lane[lane0]['nof_blocks']

        dk = k_stop - k_start
        dn = n_stop - n_start

        t = np.arange(dn * self.lane[lane0]['nffte'], dtype='float64')
        t *= (5.12e-6 * self.lane[lane0]['fftlen'] * self.lane[lane0]['nfft2int'])
        t += self.lane[lane0]['times'][n_start]

        f = np.arange(dk * self.lane[lane0]['fftlen'], dtype='float64')
        f *= (1.0 / 5.12e-6 / self.lane[lane0]['fftlen'])
        f += (k_start*max_bsn - max_bsn / 2)

        if self.verbose:
            print("Part %s\n  t: %d -> %d : dn=%d\n  f: %d -> %d : dk=%d" % (
                    part,
                    n_start, n_stop, dn,
                    k_start, k_stop, dk,
                    )
                  )



        def gather_data(fft_i, part_i):
            # allocate some space for the result
            data = np.empty((dn,
                             dk,
                             self.lane[lane0]['nffte'],
                             self.lane[lane0]['fftlen'],
                             2 if part == "XY*" else 1,
                             ),
                            dtype='float32',
                            )

            # Compute where beamlet are located in lanes
            lane_idx, beamlet_idx = np.where(  (k_start <= self.parset['Beam'][beam]['logical'])
                                             & (self.parset['Beam'][beam]['logical'] < k_stop))
            unique_lane_idx = list(set(lane_idx))
            logical = {}
            for l in unique_lane_idx:
                logical[l] = beamlet_idx[lane_idx == l]

            # Retreive data from each lanes and concatenate into data array
            k_idx = 0
            for l, b in logical.items():
                lane_dk = len(b)
                tmp = self.lane[l]['fn_data']['data'][fft_i][..., part_i]
                data[:, k_idx:k_idx+lane_dk, :, :, part_i if part == "XY*" else 0 ] = tmp[n_start:n_stop, b, :, :]
                k_idx += lane_dk
            data = data.squeeze()
            return data


        if part == 'XX*':
            data = gather_data('fft0', 0)
            data = data.transpose((0, 2, 1, 3))

        if part == 'YY*':
            data = gather_data('fft0', 1)
            data = data.transpose((0, 2, 1, 3))

        if part == "XY*":
            # check if can be optimized !!!!!!!!!!!!
            # check if can be optimized !!!!!!!!!!!!
            # check if can be optimized !!!!!!!!!!!!
            # check if can be optimized !!!!!!!!!!!!
            data = gather_data('fft1', slice(0,2))
            data = data.transpose((0, 2, 1, 3, 4)).copy()
            data = data.view("complex64").squeeze()

        if part in ('I', 'Q'):
            XX = gather_data('fft0', 0)
            data = XX.copy()
            YY = gather_data('fft0', 1)
            if part is "I":
                data += YY
            elif part is "Q":
                data -= YY
            data = data.transpose((0, 2, 1, 3))

        if part == 'U':
            data = gather_data('fft1', 0)
            data = data.transpose((0, 2, 1, 3))
            data *= 2

        if part == 'V':
            data = gather_data('fft1', 1)
            data = data.transpose((0, 2, 1, 3))
            data *= -2

        assert data is not None, \
            "No data was extracted.  Check what you asked for..."

        if fftshift:
            data = np.fft.fftshift(data, axes=(3,))

        if PFB_correction:
            # use the correction from the first lane of the beam
            # with the bold assumption that it is the same for other lanes...
            data *= self.lane[lane0]['PFB_corr']

        data = data.reshape((dn * self.lane[lane0]['nffte'],
                             dk * self.lane[lane0]['fftlen'],
                             ))

        return data, t, f




if __name__ == "__main__":

    import matplotlib.pyplot as plt
    import matplotlib.dates as md

    # precise dateformater for time y-axis labels
    y_fmt = md.DateFormatter('%Y-%m-%d\n%H:%M:%S\n+0.%f')
    # remove eol from format for bottom cursor info (will remain in y-axis labels)
    def format_coord(x, y):
        ys = ax.format_ydata(y).replace('\n', ' ')
        return 'f=%2.2f MHz t=%s' % (x, ys)

    # Solar flares
    obs_name, t_sel, f_sel, n_sel, k_sel, beam, parts, whiten = ('20190320_104900_20190320_125000_SUN_TRACKING_BHR',
                                                   #("2019-03-20 10:55:00.0", "2019-03-20 10:55:30.0"), (25e6, 58e6),
                                                   #("2019-03-20 11:10:12.0", "2019-03-20 11:10:32.0"), (25e6, 58e6),
                                                   #("2019-03-20 11:14:00.0", "2019-03-20 11:14:30.0"), (25e6, 58e6),
                                                   ("2019-03-20 11:12:42.0", "2019-03-20 11:12:51.0"), (35e6, 40e6),
                                                   None,
                                                   None,
                                                   0,
                                                   #('XX', 'YY', 'I', 'XY'),
                                                   #('XX*', 'YY*', 'XY*', 'I', 'Q', 'U', 'V'),
                                                   ('I', 'Q', 'U', 'V'),
                                                   True,
                                                   )

    obs_name, t_sel, f_sel, n_sel, k_sel, beam, parts, whiten = ('20190423_000600_20190423_084200_LSR_J1835+3259_TRACKING',
                                                  ("2019-04-23T00:07:00.0", "2019-04-23T00:08:00.0"),
                                                  #(25e6, 58e6),
                                                  (0e6, 100e6),
                                                  #(30e6, 80e6),
                                                  None,
                                                  None,
                                                  #(0, 30),
                                                  #(0, 512),
                                                  1,
                                                  ('XX*', 'YY*', 'XY*', 'I', 'Q', 'U', 'V'),
                                                  True,
                                                  )

    obs_name, t_sel, f_sel, n_sel, k_sel, beam, parts, whiten = ('20190621_214000_20190621_215200_B1919+21_TRACKING',
                                                  ("2019-06-21T21:45:05.0", "2019-06-21T21:45:10.0"),
                                                  (72.5e6, 73e6),  # (0e6, 100e6),
                                                  None,  # (0, 30),
                                                  None,  # (0, 512),
                                                  3,
                                                  ('XX*', ),  # ('XX*', 'YY*', 'XY*', 'I', 'Q', 'U', 'V'),
                                                  False,  # True,
                                                  )

    obs_name = '20190719_104400_20190719_111600_SUN_TRACKING_SINGLE_BEAM'
    t_sel = ("2019-07-19T11:06:15.0", "2019-07-19T11:06:45.0")
    f_sel = (10e6, 80e6)
    n_sel = None
    k_sel = None
    beam = 0
    parts = ('I', 'V')
    whiten = False


    my_spectra = spectra(obs_name,
                         # For local dev
                         #obs_dir="/media/data/databf2/nenufar-tf",
                         #manip_dir="/media/data/databf2/nenufar",
                         verbose=True,
                         )
    for part in parts:
        data, t_axis, f_axis = my_spectra.extract_data(beam=beam,
                                                       n=n_sel,
                                                       t=t_sel,
                                                       k=k_sel,
                                                       f=f_sel,
                                                       part=part,
                                                       # PFB_correction=False,
                                                       )

        # Compute start/stop time for display
        t0, t1 = Time(t_axis[[0, -1]], format='unix', scale='utc').plot_date

        # band equalisation/normalization
        whitening = 1.0
        if whiten:
            whitening = np.median(data, axis=0)
            whitening /= np.median(whitening)
            whitening = 1.0/whitening

        if part in ('XX*', 'YY*', 'I'):  # power data
            data_plot = 10*np.log10(data * whitening)
            noise_baseline = np.median(data_plot.flatten())
            noise_std = np.std(data_plot.flatten())

            fig1, ax = plt.subplots(1, 1)
            ax.imshow(data_plot,
                      extent=(f_axis[0]*1e-6, f_axis[-1]*1e-6, t1, t0),
                      interpolation='nearest',
                      origin='upper',
                      vmin=noise_baseline-3*noise_std,
                      vmax=noise_baseline+3*noise_std,
                      cmap="Greys_r"
                      )
            ax.yaxis.set_major_formatter(y_fmt)
            ax.format_coord = format_coord
            ax.axis('tight')
            ax.set_xlabel('Freq (MHz)')
            ax.set_ylabel('Time (s)')
            ax.set_title("10 log10 (%s)" % part)
            fig1.subplots_adjust(left=0.05, bottom=0.02,
                                 right=1, top=0.90,
                                 wspace=None, hspace=None)

        if part in ('Q', 'U', 'V'):   # polarisation data
            # get I for normalization
            I, _, _ = my_spectra.extract_data(beam=beam,
                                              n=n_sel,
                                              t=t_sel,
                                              k=k_sel,
                                              f=f_sel,
                                              part='I',
                                              )

            data_plot = data / I
            noise_std = np.std(data_plot.flatten())

            fig1, ax = plt.subplots(1, 1)
            ax.imshow(data_plot,
                      extent=(f_axis[0]*1e-6, f_axis[-1]*1e-6, t1, t0),
                      interpolation='nearest',
                      origin='upper',
                      vmin=-1, vmax=1,
                      cmap="bwr"
                      )
            ax.yaxis.set_major_formatter(y_fmt)
            ax.format_coord = format_coord
            ax.axis('tight')
            ax.set_xlabel('Freq (MHz)')
            ax.set_ylabel('Time (s)')
            ax.set_title("%s / I" % part)
            fig1.subplots_adjust(left=0.05, bottom=0.02,
                                 right=1, top=0.90,
                                 wspace=None, hspace=None)

        if part is 'XY*':    # Complex data
            angle_data = np.angle(data)
            abs_data = 10*np.log10(np.abs(data))
            noise_baseline = np.median(abs_data.flatten())
            noise_std = np.std(abs_data.flatten())

            fig1, axs = plt.subplots(1, 2, sharex=True, sharey=True)
            axs[0].imshow(abs_data,
                          extent=(f_axis[0]*1e-6, f_axis[-1]*1e-6, t1, t0),
                          interpolation='nearest',
                          origin='upper',
                          vmin=noise_baseline-3*noise_std,
                          vmax=noise_baseline+3*noise_std,
                          cmap="Greys_r"
                          )
            axs[1].imshow(angle_data,
                          extent=(f_axis[0]*1e-6, f_axis[-1]*1e-6, t1, t0),
                          interpolation='nearest',
                          origin='upper',
                          vmin=-np.pi,
                          vmax=np.pi,
                          cmap="hsv"
                          )
            for ax in axs:
                ax.yaxis.set_major_formatter(y_fmt)
                ax.format_coord = format_coord
                ax.axis('tight')
                ax.set_xlabel('Freq (MHz)')
            axs[0].set_ylabel('Time (s)')
            axs[0].set_title("10 log10 |XY|")
            axs[1].set_title("angle(XY)")
            fig1.subplots_adjust(left=0.05, bottom=0.02,
                                 right=1, top=0.90,
                                 wspace=0.01, hspace=None)


    plt.show()
