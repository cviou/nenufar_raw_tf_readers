#!/usr/bin/env python3

###############################################################################
#
# Copyright (C) 2019
# Station de Radioastronomie de Nançay,
# Observatoire de Paris, PSL Research University, CNRS, Univ. Orléans, OSUC,
# 18330 Nançay, France
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
# Author: Cedric Viou (Cedric.Viou@obs-nancay.fr)
# https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/doc/serveurs/nenufar-tf/
# https://gitlab.obspm.fr/cviou/nenufar_raw_tf_readers
#
# Description:
# Lib that can read nenufar-raw output files
#
###############################################################################



from pathlib import Path
import numpy as np
import re
import datetime as dt

Nof_pol = 2
ReIm = 2

class raw:
    def __init__(self, obs_dir, verbose=False):
        if type(obs_dir) is str:
            obs_dir = Path(obs_dir)

        self.fnames_parset = sorted(list(obs_dir.glob("*.parset")))
        self.fnames_log = sorted(list(obs_dir.glob("*.spectra.log")))
        self.fnames_raw = sorted(list(obs_dir.glob("*.raw")))


        self.raw = []
        self.dt_header = []
        self.header = []
        self.dt_block = []
        self.verbose = verbose

        for lane_idx, (fn_raw) in enumerate(self.fnames_raw):
            if self.verbose:
                print(f"Opening {fn_raw.name} ({fn_raw.stat().st_size / 1024**3:4.2f} GiB)")
                
            dt_header, header, dt_block = self.infer_data_structure(fn_raw)
            raw = self.open_raw(fn_raw, dt_header, dt_block)
            self.raw.append(raw)
            self.dt_header.append(dt_header)
            self.header.append(header)
            self.dt_block.append(dt_block)
            
            if self.verbose:
                print    ( "  Header format is:")
                for (field, *fmt) in dt_header.descr:
                    print(f"    {field:20} : {fmt}")
                print    (f"    {header['nobpb']:20} beamlets")
                print    (f"    {header['nb_samples']:20} samples/blocks")
                print    (f"    {header['bytespersample']:20} bytespersample => bit_mode = {header['bytespersample'] * 2}")
                print    ( "  Block format is:")
                for (field, *fmt) in dt_block.descr:
                    print(f"    {field:20} : {fmt}")
                print    (f"    {len(raw):20} blocs")


    def infer_data_structure(self, fn_raw):
        """Parse raw file
        """
        dt_header = np.dtype([('nobpb', 'int32'),          # NUMBER_OF_BEAMLET_PER_BANK = number of channels
                              ('nb_samples', 'int32'),     # NUMBER of SAMPLES (fftlen*nfft)
                              ('bytespersample', 'int32'), # BYTES per SAMPLE (4/8 for 8/16bits data)
                              ])

        with fn_raw.open('rb') as fd_raw:
            header = np.frombuffer(fd_raw.read(dt_header.itemsize),
                                   count=1,
                                   dtype=dt_header,
                                   )[0]

        nobpb = header['nobpb']
        nb_samples = header['nb_samples']
        bytespersample = header['bytespersample']
        bytes_in = bytespersample * nb_samples * nobpb

        if bytespersample == 4:
            bit_mode = 'int8'
        elif bytespersample == 8:
            bit_mode = 'int16'
        else:
            print(f"Unknown bytespersample: {bytespersample}")
            return None, None

        dt_block = np.dtype([('eisb', 'uint64'),
                               ('tsb', 'uint64'),
                               ('bsnb', 'uint64'),
                               ('data', bit_mode, (nb_samples * nobpb * Nof_pol * ReIm,)),
                               ])

        dt_lane_beam_chan = np.dtype([('lane', 'int32'),
                                      ('beam', 'int32'),
                                      ('chan', 'int32'),
                                      ])

        dt_header = np.dtype([('nobpb', 'int32'),          # NUMBER_OF_BEAMLET_PER_BANK = number of channels
                              ('nb_samples', 'int32'),     # NUMBER of SAMPLES (fftlen*nfft)
                              ('bytespersample', 'int32'), # BYTES per SAMPLE (4/8 for 8/16bits data)
                              ('lbc_alloc', dt_lane_beam_chan, (nobpb,)),
                              ])
        with fn_raw.open('rb') as fd_raw:
            header = np.frombuffer(fd_raw.read(dt_header.itemsize),
                                   count=1,
                                   dtype=dt_header,
                                   )[0]

        return dt_header, header, dt_block


    def open_raw(self, fn_raw, dt_header, dt_block):
        """Return a memory mapped file array"""
        # tmp = np.memmap(fn_raw.as_posix(),
        #                 dtype='int8',
        #                 mode='r',
        #                 offset=dt_header.itemsize,
        #                 )
        # # Reinterpret an integer number of dt_block-length samples as dt_block
        # nof_blocks = tmp.size * tmp.itemsize // (dt_block.itemsize)
        # data = tmp[: nof_blocks * dt_block.itemsize].view(dt_block)

        data = np.memmap(fn_raw.as_posix(),
                         dtype=dt_block,
                         mode='r',
                         offset=dt_header.itemsize,
                         )
        return data


    def write_16_as_8bit_raw(self, filename, lane=0, rs=0, start=0, end=-1):
        """Writes raw data from lane using bit_mode into a file for later reuse"""
        if type(filename) is str:
            filename = Path(filename)
        with filename.open('w') as fd_raw:
            header = self.header[lane].copy()
            assert header['bytespersample'] == 8, "raw data should be 16-bit_mode to be written in 8 bits"
            header['bytespersample'] = 4
            header.tofile(fd_raw)
            lend = len(self.raw[lane]) if end == -1 else end

            for block_idx, (eisb, tsb, bsnb, data) in enumerate(self.raw[lane][start:lend]):
                eisb.tofile(fd_raw)
                tsb.tofile(fd_raw)
                bsnb.tofile(fd_raw)
                # detect values that would be (odd + 0.5000...) after shift
                if rs == 1:
                    Cin = np.where(data & 3 == 3, 1, 0)
                elif rs > 1 :
                    Cin = np.where(  ((data & (2**(rs-1))) == (2**(rs-1))) 
                                   & ((data & (2**(rs+1)-1)) != (2**(rs-1))),
                                   1, 0)
                data = data >> rs
                data += Cin
                overflow = ((data > 127) | (data < -128))
                N_overflow = overflow.sum()
                overflow = overflow.any()
                np.clip(data, -128, 127, out=data)
                # todo
                data.astype('int8').tofile(fd_raw)

                msg = f"{block_idx:3}: {eisb:10} {tsb:12} {bsnb:7}"
                if overflow:
                    msg += f"  Overflow! ({N_overflow}/{len(data)} = {100 * N_overflow / len(data):4.2f}%)"
                print(msg)


    def __repr__(self):
        return "Raw manip dir"


    

if __name__ == "__main__":

    import matplotlib.pyplot as plt
    import matplotlib.dates as md
    import datetime as dt
    from dateutil import parser
    import sys

    plot_auto_corr = True
    plot_cross_corr = True

    if len(sys.argv) == 4:
        obs_dir = Path(sys.argv[1])
        t_start = parser.parse(sys.argv[2])
        t_stop  = parser.parse(sys.argv[3])
    else:
        sys.exit('error in input params')

    assert obs_dir.exists(), "File not found.  Check path, or try running on a nancep node"

    my_spectra = raw(obs_dir,
                     verbose=True,
                     )

    max_bsn = 200e6 / 1024
    nof_pol, ReIm = 2, 2
    header = my_spectra.header[0]
    nof_blocks = my_spectra.raw[0]['data'].shape[0]
    nobpb = header['nobpb']
    nof_sample_per_block = header['nb_samples']
    sample_size = nobpb * nof_pol * ReIm
    fftlen = 16
    nacc = 128

    # compute t axis
    blk_time  = my_spectra.raw[0]['tsb'].astype("double")
    blk_time += my_spectra.raw[0]['bsnb'].astype("double") / max_bsn
    blk_time = np.array(tuple(dt.datetime.fromtimestamp(t) for t in blk_time))

    assert blk_time[0] <= t_start < t_stop <= blk_time[-1], \
      f"Time slice selection error:\n↓ {blk_time[0]}\n↓ {t_start}\n↓ {t_stop}\n↓ {blk_time[-1]}"
    blk_start = blk_time.searchsorted(t_start) - 1
    blk_start_frac = t_start - blk_time[blk_start]
    blk_start_frac = int(blk_start_frac.total_seconds() * max_bsn)
    blk_stop  = blk_time.searchsorted(t_stop) - 1
    blk_stop_frac = t_stop - blk_time[blk_stop]
    blk_stop_frac = int(blk_stop_frac.total_seconds() * max_bsn) + 1
    nb_full_blk = blk_stop - blk_start - 1

    nof_sample_to_read = ( (nof_sample_per_block-blk_start_frac)
                         + nb_full_blk*nof_sample_per_block
                         + blk_stop_frac)
    dat = np.empty(nof_sample_to_read * sample_size,
                   dtype=my_spectra.raw[0]['data'].dtype)
    print(f"Loading {nb_full_blk + (nof_sample_per_block-blk_start_frac+blk_stop_frac) / nof_sample_per_block:.2f} blocs ({dat.size // 1024**2} MB)")

    pos = (nof_sample_per_block-blk_start_frac) * sample_size
    dat[:pos] = my_spectra.raw[0]['data'][blk_start][blk_start_frac * sample_size:]
    print(f"{0}-{pos}")
    for blk in range(nb_full_blk):
        print(f"{pos}-{pos+nof_sample_per_block * sample_size}")
        dat[pos:pos+nof_sample_per_block * sample_size] = my_spectra.raw[0]['data'][blk]
        pos += nof_sample_per_block * sample_size
    print(f"{pos}-{nof_sample_to_read*sample_size}")
    dat[pos:] = my_spectra.raw[0]['data'][blk_stop][:blk_stop_frac * sample_size]
    assert pos == len(dat)-blk_stop_frac*sample_size

    nfft = nof_sample_to_read * sample_size // (fftlen * nobpb * Nof_pol * ReIm)
    nfft = nfft // nacc * nacc
    nspec = nfft//nacc

    dat = dat[:nfft * fftlen * sample_size]
    dat.shape = (nspec, nacc, fftlen, nobpb, Nof_pol, ReIm)

    dat = dat.astype('float32').view('complex64').squeeze()
    dat = dat.transpose((4,0,1,3,2))
    DAT = np.fft.fft(dat, axis=-1)
    DAT = np.fft.fftshift(DAT, axes=-1)
    DAT = DAT.mean(axis=2)
    DAT.shape = (Nof_pol, nspec, nobpb*fftlen)
    DAT = 20*np.log10(np.abs(DAT))

    # compute f axis
    beamlet_k = np.array([lbc_alloc[-1] for lbc_alloc in header['lbc_alloc']])
    chan_sel_start, chan_sel_stop = 0, len(beamlet_k)
    
    assert chan_sel_start >=0 and chan_sel_start < len(beamlet_k), \
      f"chan_sel_start={chan_sel_start} can't index beamlet_k range [0,{len(beamlet_k)-1}]"
    assert chan_sel_stop >0 and chan_sel_stop <= len(beamlet_k), \
      f"chan_sel_stop={chan_sel_stop} can't index beamlet_k range [0,{len(beamlet_k)-1}]"

    beamlet_k = beamlet_k[chan_sel_start:chan_sel_stop]
    chan_start, chan_stop = beamlet_k[0], beamlet_k[-1]
    freq_range = (chan_start - 0.5) * 200.0/1024, (chan_stop  + 0.5) * 200.0/1024
    f = np.linspace(*freq_range, fftlen*(chan_stop - chan_start + 1), endpoint=False)


    extent = f[0], f[-1], md.date2num(t_stop), md.date2num(t_start)
    med = np.median(DAT.flatten())
    mad = np.median(np.abs(DAT.flatten()-med))
    v = (med-3*mad, med+3*mad)

    fig1, axs = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(30,15))
    for polar, ax in enumerate(axs):
        ax.imshow(DAT[polar],
                  extent=extent,
                  interpolation='nearest',
                  origin='upper',
                  vmin=v[0], vmax=v[1],
                  cmap="Greys_r"
                  )
        ax.axis('tight')
        ax.set_title(("XX","YY")[polar])
        xfmt = md.DateFormatter('%Y-%m-%d\n%H:%M:%S\n+0.%f')
        ax.yaxis.set_major_formatter(xfmt)
        ax.set_xlabel('Freq (MHz)')

    plt.show()

